#!/usr/bin/env python3

import csv
import logging
import time
from datetime import datetime

import paho.mqtt.publish as publish
import pytz
import requests
import schedule as schedule
from lxml import html

# Configuration
TOPIC_CORONA = 'corona/count'
TOPIC_CORONA_LAST_UPDATED = 'corona/last_updated'
HOSTNAME = "192.168.1.3"
LOCAL_TIMEZONE = pytz.timezone('Europe/Amsterdam')


def patients_count():
    logging.info("Requesting data from RIVM")
    page = requests.get("https://www.rivm.nl/coronavirus-kaart-van-nederland")
    tree = html.fromstring(page.content)
    numbers = tree.xpath('//div[@id="csvData"]/text()')[0].strip().splitlines()
    csv_data = csv.DictReader(numbers, delimiter=";")
    numbers = list(filter(None, [x['Aantal'] for x in csv_data]))
    patients = sum([int(n) for n in numbers])
    logging.info("Data retrieved: %d patients", patients)
    return patients


def last_updated():
    return datetime.now(LOCAL_TIMEZONE).strftime('%H:%M')


def job():
    msgs = [{'topic': TOPIC_CORONA, 'payload': patients_count(), 'retain': True},
            {'topic': TOPIC_CORONA_LAST_UPDATED, 'payload': last_updated(), 'retain': True}]
    publish.multiple(msgs, hostname=HOSTNAME)
    logging.info("Messages published: %s", msgs)


def main():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')
    logging.info("starting up")
    schedule.every().hour.do(job)
    # run on startup
    schedule.run_all()
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    main()
