# README
Quick-and-dirty contraption to parse RIVM's webpage for corona stats and post it to an mqtt topic.
Includes scheduler to run it every hour.
## Usage
1. Set your own values in `# Configuration` block in `main.py`
2. Run the following commands
```shell script
 pip install -r requirements.txt
 python main.py
```
3. Or, if you want to run through docker:
```shell script
docker-compose -H DOCKERHOST up -d
```